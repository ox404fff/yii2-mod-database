<?
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */
namespace ox404fff\moddatabase\db;

use ox404fff\moddatabase\events;
use yii\db\ActiveRecord as BaseActiveRecord;

/**
 * @package ox404fff\moddatabase\db
 *
 */
class ActiveRecord extends BaseActiveRecord
{

    /**
     * The attribute specifies whether the record is deleted
     */
    const ATTRIBUTE_IS_DELETED = 'is_deleted';

    /**
     * @var bool Is enable soft delete
     */
    public static $isSafeDelete = true;


    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        $db = parent::getDb();

        if (static::$isSafeDelete) {
            $db->commandClass = Command::className();
        }

        return $db;
    }


    /**
     * Find with add is not deleted condition
     *
     * @inheritdoc
     * 
     * @return ActiveQuery
     */
    public static function find()
    {
        $activeQuery = parent::find();
        /**
         * @var ActiveQuery $activeQuery
         */
        $activeQuery->setIsSafeDelete(static::$isSafeDelete);

        $activeQuery->setSafeDeleteAttribute(static::ATTRIBUTE_IS_DELETED);

        return $activeQuery;
    }


    /**
     * Set is deleted attribute for correct work method isDeleted
     *
     * @inheritdoc
     */
    public function delete()
    {
        if (!static::$isSafeDelete) {
            return parent::delete();
        }
        
        if ($this->isDeleted()) {
            return 0;
        }

        $result = parent::delete();
        if ($result) {
            $this->setAttribute(static::ATTRIBUTE_IS_DELETED, true);
        }
        return $result;
    }


    /**
     * Return is deleted record
     *
     * @return bool
     */
    public function isDeleted()
    {
        return (bool) $this->getAttribute(static::ATTRIBUTE_IS_DELETED);
    }

}