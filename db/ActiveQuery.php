<?
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\db;

use ox404fff\moddatabase\behaviors\SafeDeleteActiveQueryBehavior;
use ox404fff\moddatabase\events;
use yii\db\ActiveQuery as BaseQuery;
use yii\helpers\ArrayHelper;

/**
 * Set advanced events to ActiveQuery class 
 * 
 * @package ox404fff\moddatabase\db
 * 
 * @property SafeDeleteActiveQueryBehavior $safeDelete
 * 
 * @mixin SafeDeleteActiveQueryBehavior
 */
class ActiveQuery extends BaseQuery
{
    
    /**
     * Event on before create command name
     */
    const EVENT_BEFORE_CREATE_COMMAND = 'before_create_command';


    /**
     * Event on after create command name
     */
    const EVENT_AFTER_CREATE_COMMAND = 'after_create_command';


    /**
     * Attribute if record is deleted
     *
     * @var string
     */
    public $isDeletedAttribute = 'is_deleted';


    /**
     * @return array the behaviors.
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'safeDelete' => [
                'class'              => SafeDeleteActiveQueryBehavior::className(),
                'isDeletedAttribute' => $this->isDeletedAttribute,
            ]
        ]);
    }

    
    /**
     * @inheritdoc
     * 
     * @return Command
     */
    public function createCommand($db = null)
    {
        if ($this->beforeCreateCommand($db)) {
            
            $command = parent::createCommand($db);

            $this->afterCreateCommand($command, $db);
            
            return $command;
            
        } else {
            
            return null;
            
        }
    }


    /**
     * Change soft delete status
     *
     * @param bool $value
     * @return bool
     */
    public function setIsSafeDelete($value)
    {
        if ($value) {
            if ($this->getIsSafeDelete()) {
                return false;
            }
            $this->attachBehavior('safeDelete', [
                'class'              => SafeDeleteActiveQueryBehavior::className(),
                'isDeletedAttribute' => $this->isDeletedAttribute
            ]);
            return true;
        } else {
            return !is_null($this->detachBehavior('safeDelete'));
        }
    }


    /**
     * Return true if safe delete enabled, and return false if disabled
     *
     * @return bool
     */
    public function getIsSafeDelete()
    {
        return (bool) $this->getBehavior('safeDelete');
    }


    /**
     * Set soft delete attribute name
     *
     * @param $attribute
     * @return bool
     */
    public function setSafeDeleteAttribute($attribute)
    {
        $safeDeleteBehavior = $this->getBehavior('safeDelete');

        /**
         * @var SafeDeleteActiveQueryBehavior $safeDeleteBehavior;
         */
        if ($safeDeleteBehavior) {
            $this->isDeletedAttribute = $attribute;
            $safeDeleteBehavior->isDeletedAttribute = $this->isDeletedAttribute;
            return true;
        }

        return false;
    }


    /**
     * Raising before create command event. Return false if isValid Event attribute set false
     *
     * @param $db
     * @return bool
     */
    public function beforeCreateCommand($db = null)
    {
        $event = new events\BeforeCreateCommandEvent([
            'db' => $db
        ]);

        $this->trigger(static::EVENT_BEFORE_CREATE_COMMAND, $event);

        return $event->isValid;
    }


    /**
     * Raising after create command event
     * 
     * @param $db
     * @param $command
     */
    public function afterCreateCommand($command, $db = null)
    {
        $this->trigger(static::EVENT_AFTER_CREATE_COMMAND, new events\AfterCreateCommandEvent([
            'db'      => $db,
            'command' => $command
        ]));
    }

}