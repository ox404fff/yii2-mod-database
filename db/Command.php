<?
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\db;

use ox404fff\moddatabase\events;
use yii\db\Command as BaseCommand;

/**
 * Class Command
 * 
 * @package ox404fff\moddatabase\db
 */
class Command extends BaseCommand
{
    

    /**
     * Event on before delete command execute
     */
    const EVENT_BEFORE_DELETE = 'before_delete';

    /**
     * Event on after delete command execute
     */
    const EVENT_AFTER_DELETE = 'after_delete';

    /**
     * Event on before update command execute
     */
    const EVENT_BEFORE_UPDATE = 'before_update';

    /**
     * Event on after update command execute
     */
    const EVENT_AFTER_UPDATE = 'after_update';
    
    
    /**
     * @inheritdoc 
     */
    public function delete($table, $condition = '', $params = [])
    {
        $event = $this->beforeDelete($table, $condition, $params);

        if (!$event->isValid) {

            return null;

        }

        if (!$event->isCommandReplaced()) {

            $result = $this->executeDelete(
                $event->table,
                $event->condition,
                $event->params
            );

        } else {

            $result = $event->getReplaceCommand();

        }

        $this->afterDelete(
            $event->table,
            $event->condition,
            $event->params
        );

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function update($table, $columns, $condition = '', $params = [])
    {

        $event = $this->beforeUpdate($table, $columns, $condition, $params);

        if (!$event->isValid) {

            return null;

        }

        if (!$event->isCommandReplaced()) {

            $result = $this->executeUpdate(
                $event->table,
                $event->columns,
                $event->condition,
                $event->params
            );

        } else {

            $result = $event->getReplaceCommand();

        }
        
        $this->afterUpdate(
            $event->table,
            $event->columns,
            $event->condition,
            $event->params
        );

        return $result;
    }


    /**
     * Execute original update command
     * 
     * @param $table
     * @param $columns
     * @param string $condition
     * @param array $params
     * @return $this
     */
    public function executeUpdate($table, $columns, $condition = '', $params = [])
    {
        return parent::update($table, $columns, $condition, $params);
    }


    /**
     * Execute original delete command
     * 
     * @param $table
     * @param string $condition
     * @param array $params
     *
     * @return $this
     */
    protected function executeDelete($table, $condition = '', $params = [])
    {
        return parent::delete($table, $condition, $params);
    }

    /**
     * Raising before delete command execute. Return false if isValid Event attribute set false
     * 
     * @param $table
     * @param string $condition
     * @param $params
     * @return events\DeleteCommandEvent
     */
    public function beforeDelete($table, $condition = '', $params = [])
    {
        $event = new events\DeleteCommandEvent([
            'table'     => $table,
            'condition' => $condition,
            'params'    => $params,
        ]);

        $this->trigger(self::EVENT_BEFORE_DELETE, $event);

        return $event;
    }


    /**
     * Raising after delete command execute 
     * 
     * @param $table
     * @param string $condition
     * @param $params
     */
    public function afterDelete($table, $condition = '', $params = [])
    {
        $event = new events\DeleteCommandEvent([
            'table'     => $table,
            'condition' => $condition,
            'params'    => $params,
        ]);

        $this->trigger(self::EVENT_AFTER_DELETE, $event);
    }


    /**
     * Raising before update command execute. Return false if isValid Event attribute set false
     * 
     * @param $table
     * @param $columns
     * @param string $condition
     * @param array $params
     * @return events\UpdateCommandEvent
     */
    public function beforeUpdate($table, $columns, $condition = '', $params = [])
    {
        $event = new events\UpdateCommandEvent([
            'table'     => $table,
            'columns'   => $columns,
            'condition' => $condition,
            'params'    => $params,
        ]);

        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);

        return $event;
    }


    /**
     * Raising after update command execute.
     * 
     * @param $table
     * @param $columns
     * @param string $condition
     * @param array $params
     */
    public function afterUpdate($table, $columns, $condition = '', $params = [])
    {
        $event = new events\UpdateCommandEvent([
            'table'     => $table,
            'columns'   => $columns,
            'condition' => $condition,
            'params'    => $params,
        ]);

        $this->trigger(self::EVENT_AFTER_UPDATE, $event);
    }
}