<?php

namespace ox404fff\moddatabase;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{

    public function bootstrap($app)
    {
        \Yii::$container->set('yii\db\ActiveQuery', ['class' => 'ox404fff\moddatabase\db\ActiveQuery']);
    }

}