<?php

namespace ox404fff\moddatabase\behaviors;

use ox404fff\moddatabase\db\Command;
use ox404fff\moddatabase\events;
use ox404fff\moddatabase\exceptions\SafeDeleteCommandBehaviorException;
use yii\base\Behavior;

/**
 * Reading date without safe-deleting records
 *
 * Class StaticCacheBehavior
 * @package app\base\behaviors
 */
class SafeDeleteCommandBehavior extends Behavior
{

    /**
     * Attribute if is deleted record
     *
     * @var string
     */
    public $isDeletedAttribute = 'is_deleted';
    
    
    /**
     * @var Command
     */
    public $owner;


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function events()
    {
        return [
            Command::EVENT_BEFORE_DELETE => 'beforeDelete'
        ];
    }


    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        if ($owner instanceof Command) {
            parent::attach($owner);
        } else {
            throw new SafeDeleteCommandBehaviorException('Owner must be instance of Command');
        }
    }


    /**
     * Replace delete command on update command
     *
     * @param events\DeleteCommandEvent $event
     */
    public function beforeDelete(events\DeleteCommandEvent $event)
    {
        $command = $this->owner->executeUpdate(
            $event->table,
            [$this->isDeletedAttribute => true],
            $event->condition,
            $event->params
        );

        $event->setReplaceCommand($command);
    }
    
}
