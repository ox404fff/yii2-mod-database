<?php

namespace ox404fff\moddatabase\behaviors;

use ox404fff\moddatabase\db\ActiveQuery;
use ox404fff\moddatabase\events;
use ox404fff\moddatabase\exceptions\SafeDeleteActiveQueryBehaviorException;
use yii\base\Behavior;

/**
 * Reading date without safe-deleting records
 *
 * Class StaticCacheBehavior
 * @package app\base\behaviors
 */
class SafeDeleteActiveQueryBehavior extends Behavior
{

    /**
     * @var ActiveQuery
     */
    public $owner;

    /**
     * Attribute if is deleted record
     * 
     * @var string
     */
    public $isDeletedAttribute = 'is_deleted';
    

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function events()
    {
        return [
            ActiveQuery::EVENT_BEFORE_CREATE_COMMAND => 'beforeCreateCommand',
            ActiveQuery::EVENT_AFTER_CREATE_COMMAND  => 'afterCreateCommand',
        ];
    }


    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        if ($owner instanceof ActiveQuery) {
            parent::attach($owner);
        } else {
            throw new SafeDeleteActiveQueryBehaviorException('Owner must be instance of ActiveQuery');
        }
    }


    /**
     * On before create command, add is not deleted condition
     */
    public function beforeCreateCommand()
    {
        $this->_modifyCondition($this->owner);
    }


    /**
     * On after create command, attach events
     *
     * @param events\AfterCreateCommandEvent $event
     */
    public function afterCreateCommand(events\AfterCreateCommandEvent $event)
    {
        $event->command->attachBehavior('safeDelete', [
            'class'              => SafeDeleteCommandBehavior::className(),
            'isDeletedAttribute' => $this->isDeletedAttribute
        ]);
    }
    


    /**
     * Exclude deleted records from selection
     * 
     * @param ActiveQuery $activeQuery
     */
    private function _modifyCondition(ActiveQuery $activeQuery)
    {
        $activeQuery->andWhere([$this->isDeletedAttribute => false]);
    }
    
}
