<?
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\test\unit\db;

use ox404fff\moddatabase\events;
use ox404fff\moddatabase\db\Command;
use yii\base\Behavior;
use yii\codeception\TestCase;

/**
 * Class CommandTest
 * @package ox404fff\moddatabase\test
 */
class CommandTest extends TestCase
{
    


    public function testDelete()
    {
        $invalidEvent = new events\DeleteCommandEvent([
            'isValid'   => false,
            'table'     => 'event_attribute_table',
            'condition' => 'event_attribute_condition',
            'params'    => 'event_attribute_params',
        ]);

        $validEvent = new events\DeleteCommandEvent([
            'isValid'   => true,
            'table'     => 'event_attribute_table',
            'condition' => 'event_attribute_condition',
            'params'    => 'event_attribute_params',
        ]);

        /******************************************************************************
         *
         * On success delete and safe delete enabled,
         * afterDelete method should be called once
         *
         ******************************************************************************/
        $commandForSuccessDeleting = $this->getMockBuilder(Command::className())
            ->setMethods(['beforeDelete', 'afterDelete', 'executeDelete'])
            ->getMock();


        $commandForSuccessDeleting
            ->expects($this->once())->method('beforeDelete')->will($this->returnValue($validEvent));

        $commandForSuccessDeleting
            ->expects($this->once())->method('executeDelete');

        $commandForSuccessDeleting
            ->expects($this->once())->method('afterDelete');

        /**
         * @var Command $commandForSuccessDeleting
         */
        $commandForSuccessDeleting->delete('string');


        /******************************************************************************
         *
         * On canceled delete and safe delete enabled,
         * afterDelete method never call
         *
         ******************************************************************************/
        $commandForCanceledDeleting = $this->getMockBuilder(Command::className())
            ->setMethods(['beforeDelete', 'afterDelete', 'executeDelete'])
            ->getMock();


        $commandForCanceledDeleting
            ->expects($this->once())->method('beforeDelete')->will($this->returnValue($invalidEvent));

        $commandForCanceledDeleting
            ->expects($this->never())->method('afterDelete');

        $commandForCanceledDeleting
            ->expects($this->never())->method('executeDelete');

        /**
         * @var Command $commandForCanceledDeleting
         */
        $commandForCanceledDeleting->delete('string');


        /******************************************************************************
         *
         * Event attributes must my transported to executeDelete and afterDelete methods
         *
         ******************************************************************************/
        $commandCustomisable = $this->getMockBuilder(Command::className())
            ->setMethods(['beforeDelete', 'afterDelete', 'executeDelete'])
            ->getMock();


        $commandCustomisable
            ->expects($this->once())->method('beforeDelete')->will($this->returnValue($validEvent));

        $commandCustomisable
            ->expects($this->once())->method('executeDelete')->will($this->returnCallback(function ($table, $condition, $params) {
                $this->assertEquals($table, 'event_attribute_table');
                $this->assertEquals($condition, 'event_attribute_condition');
                $this->assertEquals($params, 'event_attribute_params');
            }));

        $commandCustomisable
            ->expects($this->once())->method('afterDelete')->will($this->returnCallback(function ($table, $condition, $params) {
                $this->assertEquals($table, 'event_attribute_table');
                $this->assertEquals($condition, 'event_attribute_condition');
                $this->assertEquals($params, 'event_attribute_params');
            }));

        /**
         * @var Command $commandCustomisable
         */
        $commandCustomisable->delete('string', 'string', []);

        /******************************************************************************
         *
         * Command must be replaceable
         *
         ******************************************************************************/
        $otherCommand = $this->getMockBuilder(Command::className())
            ->setMethods(['getIsOtherCommand'])
            ->getMock();
        $otherCommand->expects($this->any())->method('getIsOtherCommand')->will($this->returnValue(true));
        /**
         * @var Command $otherCommand
         */

        $commandReplaceable = $this->getMockBuilder(Command::className())
            ->setMethods(['executeDelete', 'afterDelete'])
            ->getMock();
        $commandReplaceable
            ->expects($this->once())
            ->method('afterDelete');
        /**
         * @var Command $commandReplaceable
         */
        $commandReplaceable->on(Command::EVENT_BEFORE_DELETE, function(events\DeleteCommandEvent $event) use ($otherCommand) {
            $event->setReplaceCommand($otherCommand);
        });

        $resultCommand = $commandReplaceable->delete('');

        $this->assertTrue($resultCommand->getIsOtherCommand());

    }


    public function testUpdate()
    {
        $validEvent = new events\UpdateCommandEvent([
            'isValid'   => true,
            'table'     => 'event_attribute_table',
            'condition' => 'event_attribute_condition',
            'params'    => 'event_attribute_params',
            'columns'    => 'event_attribute_columns',
        ]);
        $invalidEvent = new events\UpdateCommandEvent([
            'isValid'   => false,
            'table'     => 'event_attribute_table',
            'condition' => 'event_attribute_condition',
            'params'    => 'event_attribute_params',
            'columns'    => 'event_attribute_columns',
        ]);


        /******************************************************************************
         *
         * On success delete and safe delete enabled,
         * afterDelete method should be called once
         *
         ******************************************************************************/
        $commandForSuccessUpdating = $this->getMockBuilder(Command::className())
            ->setMethods(['beforeUpdate', 'afterUpdate', 'executeUpdate'])
            ->getMock();


        $commandForSuccessUpdating
            ->expects($this->once())->method('beforeUpdate')->will($this->returnValue($validEvent));

        $commandForSuccessUpdating
            ->expects($this->once())->method('afterUpdate');

        $commandForSuccessUpdating
            ->expects($this->once())->method('executeUpdate');
        /**
         * @var Command $commandForSuccessUpdating
         */
        $commandForSuccessUpdating->update('', []);


        /******************************************************************************
         *
         * On success delete and safe delete enabled,
         * afterDelete method should be called once
         *
         **/
        $commandForCanceledUpdating = $this->getMockBuilder(Command::className())
            ->setMethods(['beforeUpdate', 'afterUpdate', 'executeUpdate'])
            ->getMock();


        $commandForCanceledUpdating
            ->expects($this->once())->method('beforeUpdate')->will($this->returnValue($invalidEvent));

        $commandForCanceledUpdating
            ->expects($this->never())->method('afterUpdate');

        $commandForCanceledUpdating
            ->expects($this->never())->method('executeUpdate');
        /**
         * @var Command $commandForCanceledUpdating
         */
        $commandForCanceledUpdating->update('', []);


        /******************************************************************************
         *
         * Event attributes must my transported to executeUpdate and afterUpdate methods
         *
         ******************************************************************************/
        $commandCustomisable = $this->getMockBuilder(Command::className())
            ->setMethods(['beforeUpdate', 'afterUpdate', 'executeUpdate'])
            ->getMock();


        $commandCustomisable
            ->expects($this->once())->method('beforeUpdate')->will($this->returnValue($validEvent));

        $commandCustomisable
            ->expects($this->once())->method('executeUpdate')->will($this->returnCallback(function ($table, $columns, $condition, $params) {
                $this->assertEquals($table, 'event_attribute_table');
                $this->assertEquals($condition, 'event_attribute_condition');
                $this->assertEquals($params, 'event_attribute_params');
                $this->assertEquals($columns, 'event_attribute_columns');
            }));

        $commandCustomisable
            ->expects($this->once())->method('afterUpdate')->will($this->returnCallback(function ($table, $columns, $condition, $params) {
                $this->assertEquals($table, 'event_attribute_table');
                $this->assertEquals($condition, 'event_attribute_condition');
                $this->assertEquals($params, 'event_attribute_params');
                $this->assertEquals($columns, 'event_attribute_columns');
            }));

        /**
         * @var Command $commandCustomisable
         */
        $commandCustomisable->update('string', 'string', []);


        /******************************************************************************
         *
         * Command must be replaceable
         *
         ******************************************************************************/
        $otherCommand = $this->getMockBuilder(Command::className())
            ->setMethods(['getIsOtherCommand'])
            ->getMock();
        $otherCommand->expects($this->any())->method('getIsOtherCommand')->will($this->returnValue(true));
        /**
         * @var Command $otherCommand
         */

        $commandReplaceable = $this->getMockBuilder(Command::className())
            ->setMethods(['executeUpdate', 'afterUpdate'])
            ->getMock();
        $commandReplaceable
            ->expects($this->once())
            ->method('afterUpdate');
        /**
         * @var Command $commandReplaceable
         */
        $commandReplaceable->on(Command::EVENT_BEFORE_UPDATE, function(events\UpdateCommandEvent $event) use ($otherCommand) {
            $event->setReplaceCommand($otherCommand);
        });

        $resultCommand = $commandReplaceable->update('', []);

        $this->assertTrue($resultCommand->getIsOtherCommand());
    }


    public function testBeforeDelete()
    {
        $eventHandler = $this->getMockBuilder(Behavior::className())
            ->setMethods(['beforeDelete'])
            ->getMock();

        $eventHandler->expects($this->once())->method('beforeDelete');

        $command = new Command();
        $command->on(Command::EVENT_BEFORE_DELETE, [$eventHandler, 'beforeDelete']);

        $resultEventHandler = $command->beforeDelete('');

        $this->assertInstanceOf(events\DeleteCommandEvent::className(), $resultEventHandler);
    }


    public function testAfterDelete()
    {
        $eventHandler = $this->getMockBuilder(Behavior::className())
            ->setMethods(['afterDelete'])
            ->getMock();

        $eventHandler->expects($this->once())->method('afterDelete');

        $command = new Command();
        $command->on(Command::EVENT_AFTER_DELETE, [$eventHandler, 'afterDelete']);

        $command->afterDelete('');
    }


    public function testBeforeUpdate()
    {
        $eventHandler = $this->getMockBuilder(Behavior::className())
            ->setMethods(['beforeUpdate'])
            ->getMock();

        $eventHandler->expects($this->once())->method('beforeUpdate');

        $command = new Command();

        $command->on(Command::EVENT_BEFORE_UPDATE, [$eventHandler, 'beforeUpdate']);

        $resultEventHandler = $command->beforeUpdate('', []);

        $this->assertInstanceOf(events\UpdateCommandEvent::className(), $resultEventHandler);
    }


    public function testAfterUpdate()
    {
        $eventHandler = $this->getMockBuilder(Behavior::className())
            ->setMethods(['afterUpdate'])
            ->getMock();

        $eventHandler->expects($this->once())->method('afterUpdate');

        $command = new Command();

        $command->on(Command::EVENT_AFTER_UPDATE, [$eventHandler, 'afterUpdate']);

        $command->afterUpdate('', []);
    }


}