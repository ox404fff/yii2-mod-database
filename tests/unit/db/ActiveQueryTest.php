<?
/**
 * @link https://github.com/ox404fff/base/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\test\unit\db;

use ox404fff\moddatabase\behaviors\SafeDeleteActiveQueryBehavior;
use ox404fff\moddatabase\db\ActiveQuery;
use ox404fff\moddatabase\db\ActiveRecord;
use ox404fff\moddatabase\db\Command;
use yii\base\Behavior;
use yii\codeception\TestCase;

/**
 * Class ActiveQueryTest
 * @package ox404fff\moddatabase
 */
class ActiveQueryTest extends TestCase
{


    public function testCreateCommand()
    {
        /**
         * When before create command return true
         */
        $activeQueryCreateCommandSuccess = $this->getMockBuilder(ActiveQuery::className())
            ->setConstructorArgs([ActiveRecord::className()])
            ->setMethods(['beforeCreateCommand', 'afterCreateCommand'])
            ->getMock();

        $activeQueryCreateCommandSuccess
            ->expects($this->once())->method('beforeCreateCommand')->will($this->returnValue(true));
        $activeQueryCreateCommandSuccess
            ->expects($this->once())->method('afterCreateCommand');

        /**
         * @var ActiveQuery $activeQueryCreateCommandSuccess
         */
        $activeQueryCreateCommandSuccess->createCommand();


        /**
         * When before create command return false
         */
        $activeQueryCreateCommandCanceled = $this->getMockBuilder(ActiveQuery::className())
            ->setConstructorArgs([ActiveRecord::className()])
            ->setMethods(['beforeCreateCommand', 'afterCreateCommand'])
            ->getMock();

        $activeQueryCreateCommandCanceled
            ->expects($this->once())->method('beforeCreateCommand')->will($this->returnValue(false));
        $activeQueryCreateCommandCanceled
            ->expects($this->never())->method('afterCreateCommand');

        /**
         * @var ActiveQuery $activeQueryCreateCommandCanceled
         */
        $activeQueryCreateCommandCanceled->createCommand();
    }
    

    public function testBeforeCreateCommand()
    {
        $eventHandler = $this->getMockBuilder(Behavior::className())
            ->setMethods(['beforeCreateCommand'])
            ->getMock();

        $eventHandler->expects($this->once())->method('beforeCreateCommand');

        $activeQuery = $this->_createModel();
        $activeQuery->on(ActiveQuery::EVENT_BEFORE_CREATE_COMMAND, [$eventHandler, 'beforeCreateCommand']);
        $activeQuery->beforeCreateCommand();
    }


    public function testAfterCreateCommand()
    {
        $eventHandler = $this->getMockBuilder(Behavior::className())
            ->setMethods(['afterCreateCommand'])
            ->getMock();

        $eventHandler->expects($this->once())->method('afterCreateCommand');

        $activeQuery = $this->_createModel();
        $activeQuery->on(ActiveQuery::EVENT_AFTER_CREATE_COMMAND, [$eventHandler, 'afterCreateCommand']);

        $mockedCommand = $this->getMockBuilder(Command::className())
            ->disableOriginalConstructor()
            ->setMethods(null)
            ->getMock();
        $activeQuery->afterCreateCommand($mockedCommand);
    }



    public function testSetIsSafeDelete()
    {
        $activeQuery = $this->_createModel();
        $this->assertInstanceOf(Behavior::className(), $activeQuery->getBehavior('safeDelete'));

        $this->assertTrue($activeQuery->setIsSafeDelete(false), 'Unset soft delete must be return true');
        $this->assertFalse($activeQuery->setIsSafeDelete(false), 'Repeatedly unset soft delete must be return false');

        $this->assertEmpty($activeQuery->getBehavior('safeDelete'));

        $this->assertTrue($activeQuery->setIsSafeDelete(true), 'Set soft delete must be return true');
        $this->assertFalse($activeQuery->setIsSafeDelete(true), 'Repeatedly set soft delete must be return true');

        $this->assertInstanceOf(Behavior::className(), $activeQuery->getBehavior('safeDelete'));
    }


    public function testGetIsSafeDelete()
    {
        $activeQuery = $this->_createModel();
        $this->assertTrue($activeQuery->getIsSafeDelete(),
            'Safe delete must be enabled by default');

        $activeQuery->setIsSafeDelete(false);
        $this->assertFalse($activeQuery->getIsSafeDelete(),
            'Safe delete must be disabled after call setIsSafeDelete(false)');
    }
    

    public function testSetSafeDeleteAttribute()
    {
        $anotherAttributeName = 'another_attribute_name';

        $activeQuery = $this->_createModel();
        $this->assertTrue($activeQuery->setSafeDeleteAttribute($anotherAttributeName),
            'Setting safe delete attribute must return true');

        $behavior = $activeQuery->getBehavior('safeDelete');

        /**
         * @var SafeDeleteActiveQueryBehavior $behavior
         */
        $this->assertEquals($behavior->isDeletedAttribute, $anotherAttributeName);
        $this->assertEquals($activeQuery->isDeletedAttribute, $anotherAttributeName);

        $activeQuery->setIsSafeDelete(false);
        $this->assertFalse($activeQuery->setSafeDeleteAttribute($anotherAttributeName),
            'Setting safe delete attribute must return false if safe deleting is disabled');
    }


    /**
     * @return ActiveQuery
     */
    private function _createModel()
    {
        $activeQuery = new ActiveQuery('ox404fff\moddatabase\db\ActiveRecord');

        return $activeQuery;
    }

}