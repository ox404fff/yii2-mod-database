<?
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\test\unit\db;

use ox404fff\moddatabase\db\ActiveQuery;
use ox404fff\moddatabase\db\ActiveRecord;
use ox404fff\moddatabase\db\Command;
use yii\db\Command as OriginalCommand;
use yii\codeception\TestCase;

/**
 * Class ActiveRecordTest
 * @package ox404fff\moddatabase\test
 */
class ActiveRecordTest extends TestCase
{


    public function testGetDbOnSafeDeleteEnabled()
    {
        $model = new ActiveRecord();

        $this->assertEquals($model->getDb()->commandClass, Command::className(),
            'If safe delete enabled, command class must be equals '.Command::className());

    }


    public function testGetDbOnSafeDeleteDisabled()
    {
        $model = new ActiveRecord();
        $model::$isSafeDelete = false;

        $this->assertEquals($model->getDb()->commandClass, OriginalCommand::className(),
            'If safe delete disabled, command class must be equals original command class');
    }

    
    public function testFindOnSafeDeleteEnabled()
    {
        $model = new ActiveRecord();
        $model::$isSafeDelete = true;
        $activeQuery = $model::find();
        $this->assertInstanceOf(ActiveQuery::className(), $activeQuery);

        $this->assertTrue($activeQuery->getIsSafeDelete(), 'Safe delete behavior must be enabled in ActiveQuery class');
    }


    public function testFindOnSafeDeleteDisabled()
    {
        $model = new ActiveRecord();
        $model::$isSafeDelete = false;

        $activeQuery = $model::find();
        $this->assertInstanceOf(ActiveQuery::className(), $activeQuery);

        $this->assertFalse($activeQuery->getIsSafeDelete(), 'Safe delete behavior must be disabled in ActiveQuery class');
    }


    public function testDeleteOnSafeDeleteEnabled()
    {
        $model= $this->_createMockedModel();
        $model::$isSafeDelete = true;

        $model->setAttribute(ActiveRecord::ATTRIBUTE_IS_DELETED, false);

        $result = $model->delete();
        $this->assertEquals($result, 1);

        $this->assertTrue($model->getAttribute(ActiveRecord::ATTRIBUTE_IS_DELETED));

        $result = $model->delete();
        $this->assertEquals($result, 0);
    }


    public function testDeleteOnSafeDeleteDisabled()
    {
        $model= $this->_createMockedModel();
        $model::$isSafeDelete = false;

        $model->setAttribute(ActiveRecord::ATTRIBUTE_IS_DELETED, 'any value for is deleted');

        $result = $model->delete();
        $this->assertEquals($result, 1);

        $this->assertEquals($model->getAttribute(ActiveRecord::ATTRIBUTE_IS_DELETED), 'any value for is deleted');
    }
    

    public function testIsDeleted()
    {
        $model = $this->getMockBuilder(ActiveRecord::className())
            ->setMethods(['attributes'])
            ->getMock();

        $model->expects($this->any())->method('attributes')->will($this->returnValue([
            ActiveRecord::ATTRIBUTE_IS_DELETED
        ]));

        /**
         * @var ActiveRecord $model
         */
        $model->setAttribute(ActiveRecord::ATTRIBUTE_IS_DELETED, false);

        $this->assertFalse($model->isDeleted());

        $model->setAttribute(ActiveRecord::ATTRIBUTE_IS_DELETED, true);

        $this->assertTrue($model->isDeleted());
    }


    /**
     * Created mocked model without real execute deleteInternal method
     * and add is_deleted and deleted_at attributes
     *
     * @return ActiveRecord
     */
    private function _createMockedModel()
    {
        $model = $this->getMockBuilder(ActiveRecord::className())
            ->setMethods(['deleteInternal', 'attributes'])
            ->getMock();

        $model->expects($this->any())->method('deleteInternal')->will($this->returnValue(1));

        $model->expects($this->any())->method('attributes')->will($this->returnValue([
            ActiveRecord::ATTRIBUTE_IS_DELETED
        ]));

        return $model;
    }

}