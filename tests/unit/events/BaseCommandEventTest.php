<?
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\test\unit\db;

use ox404fff\moddatabase\db\Command;
use ox404fff\moddatabase\events;
use yii\base\ErrorException;
use yii\codeception\TestCase;

/**
 * Class BaseCommandEventTest
 * @package ox404fff\moddatabase\test
 */
class BaseCommandEventTest extends TestCase
{

    /**
     * @TODO develop me if getter method become more powerful
     */
    public function testGetOriginalConfig()
    {

    }


    /**
     * @TODO develop me if setter method become more powerful
     */
    public function testSetOriginalConfig()
    {

    }

    /**
     * @TODO develop me if getter method become more powerful
     */
    public function testGetReplaceCommand()
    {

    }



    public function test__construct()
    {
        /**
         * On creating event original config must be saved
         */
        $eventInstance = $this->getMockBuilder(events\BaseCommandEvent::className())
            ->setMethods(['setOriginalConfig'])
            ->getMock();

        $eventInstance->expects($this->once())->method('setOriginalConfig');

        /**
         * @var events\BaseCommandEvent $eventInstance
         */
        $eventInstance->__construct();

    }


    public function testSetReplaceCommand()
    {
        /**
         * On creating event original config must be saved
         */
        $eventInstance = $this->getMockBuilder(events\BaseCommandEvent::className())
            ->setMethods(null)
            ->getMock();

        /**
         * @var events\BaseCommandEvent $eventInstance
         */
        $eventInstance->setReplaceCommand(new Command());


        $constraintIsExecuted = false;
        try {
            $eventInstance->setReplaceCommand(false);
        } catch (ErrorException $e) {
            $constraintIsExecuted = true;
        }

        $this->assertTrue($constraintIsExecuted);
    }


    public function testIsCommandReplaced()
    {
        /**
         * On creating event original config must be saved
         */
        $eventInstance = $this->getMockBuilder(events\BaseCommandEvent::className())
            ->setMethods(null)
            ->getMock();

        /**
         * @var events\BaseCommandEvent $eventInstance
         */
        $this->assertFalse($eventInstance->isCommandReplaced());

        $eventInstance->setReplaceCommand(new Command());

        $this->assertTrue($eventInstance->isCommandReplaced());
    }


}