<?php

namespace ox404fff\moddatabase\test\behaviors;

use ox404fff\moddatabase\behaviors\SafeDeleteCommandBehavior;
use ox404fff\moddatabase\db\Command;
use ox404fff\moddatabase\events;
use ox404fff\moddatabase\exceptions\SafeDeleteCommandBehaviorException;
use yii\base\Component;
use yii\codeception\TestCase;

/**
 * Reading date without safe-deleting records
 *
 * Class StaticCacheBehavior
 * @package app\base\behaviors
 */
class SafeDeleteCommandBehaviorTest extends TestCase
{


    public function testAttach()
    {
        $notActiveRecordInstance = new Component();

        try {
            $notActiveRecordInstance->attachBehavior('safeDelete', [
                'class' => SafeDeleteCommandBehavior::className(),
            ]);

            $isThrowsException = false;

        } catch (SafeDeleteCommandBehaviorException $e) {
            $isThrowsException = true;
        }

        $this->assertTrue($isThrowsException,
            'On try attach behavior to class not instanced of '.Command::className().' must throws exception'
        );
    }


    public function testBeforeDelete()
    {
        $command = $this->getMockBuilder(Command::className())
            ->setMethods([
                'executeDelete', 'executeUpdate',
                'beforeUpdate', 'afterUpdate', 'afterDelete'
            ])
            ->getMock();

        /**
         * Test is original events works
         */
        $command->expects($this->never())->method('beforeUpdate');
        $command->expects($this->never())->method('afterUpdate');

        $command->expects($this->once())->method('afterDelete');

        /**
         * test is method delete replaced on update
         */
        $command->expects($this->never())->method('executeDelete');
        $command->expects($this->once())->method('executeUpdate')->will($this->returnCallback(function($table, $columns, $condition, $params) use ($command) {

            $this->assertEquals($table, 'arg_table_name');
            $this->assertEquals($condition, 'arg_condition');
            $this->assertEquals($params, 'arg_params');

            return $command;
        }));

        /**
         * @var Command $command
         */
        $command->attachBehavior('safeDelete', [
            'class' => SafeDeleteCommandBehavior::className(),
        ]);

        $command->delete('arg_table_name', 'arg_condition', 'arg_params');
    }

}
