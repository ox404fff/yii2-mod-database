<?php

namespace ox404fff\moddatabase\test\behaviors;

use ox404fff\moddatabase\behaviors\SafeDeleteActiveQueryBehavior;
use ox404fff\moddatabase\db\ActiveQuery;
use ox404fff\moddatabase\db\ActiveRecord;
use ox404fff\moddatabase\events;
use ox404fff\moddatabase\exceptions\SafeDeleteActiveQueryBehaviorException;
use yii\base\Component;
use yii\codeception\TestCase;

/**
 * Reading data without safe-deleting records
 *
 * Class StaticCacheBehavior
 * @package app\base\behaviors
 */
class SafeDeleteActiveQueryBehaviorTest extends TestCase
{

    public function testBehaviors()
    {
        $activeQuery = $this->getMockBuilder(ActiveQuery::className())
            ->setConstructorArgs([ActiveRecord::className()])
            ->setMethods(null)
            ->getMock();

        /**
         * @var ActiveQuery $activeQuery
         */
        $this->assertNotEmpty($activeQuery->getBehavior('safeDelete'),
            'Is deleted behavior must be attached to active record class');
    }


    public function testAttach()
    {
        $notActiveRecordInstance = new Component();

        try {
            $notActiveRecordInstance->attachBehavior('safeDelete', [
                'class' => SafeDeleteActiveQueryBehavior::className(),
            ]);

            $isThrowsException = false;

        } catch (SafeDeleteActiveQueryBehaviorException $e) {
            $isThrowsException = true;
        }

        $this->assertTrue($isThrowsException,
            'On try attach behavior to class not instanced of '.ActiveQuery::className().' must throws exception'
        );
    }

    public function testBeforeCreateCommand()
    {

        $activeQuery = $this->getMockBuilder(ActiveQuery::className())
            ->setConstructorArgs([ActiveRecord::className()])
            ->setMethods(['_beforeCreateCommand', '_afterCreateCommand'])
            ->getMock();

        /**
         * @var ActiveQuery $activeQuery
         */
        $activeQuery->createCommand();

        $activeQuery->setIsSafeDelete(true);

        $isDeletedAttribute = $activeQuery->isDeletedAttribute;

        $this->assertArrayHasKey($isDeletedAttribute, $activeQuery->where,
            'Where condition must has key is deleted');

        $this->assertFalse($activeQuery->where[$isDeletedAttribute],
            'Is deleted attribute in where condition should be equal false');


    }


    public function testAfterCreateCommand()
    {

        $activeQuery = $this->getMockBuilder(ActiveQuery::className())
            ->setConstructorArgs([ActiveRecord::className()])
            ->setMethods(null)
            ->getMock();

        /**
         * @var ActiveQuery $activeQuery
         */
        $activeQuery->setIsSafeDelete(true);

        $command = $activeQuery->createCommand();

        /**
         * Safe Delete command behavior must be attached
         */
        $this->assertNotEmpty($command->getBehavior('safeDelete'));
    }
    
}
