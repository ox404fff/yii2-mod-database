<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\events;

use ox404fff\moddatabase\db\Command;

/**
 * Event on before create command in ActiveQuery class
 * 
 * Class AfterCreateCommandEvent
 * @package ox404fff\moddatabase\db
 */
class AfterCreateCommandEvent extends BaseCreateCommandEvent
{

    /**
     * @var Command
     */
    public $command;
    
}
