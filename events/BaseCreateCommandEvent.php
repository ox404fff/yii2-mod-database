<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\events;

use yii\base\ModelEvent;
use yii\db\Connection;

/**
 * Base event on create command in ActiveQuery class
 *
 * Class CreateCommandEvent
 * @package ox404fff\moddatabase\db
 */
abstract class BaseCreateCommandEvent extends ModelEvent
{

    /**
     * @var Connection
     */
    public $db;
    
}
