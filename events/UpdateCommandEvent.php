<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\events;


/**
 * Event when updating in Command class
 *
 * Class BaseUpdateCommandEvent
 * @package ox404fff\moddatabase\db
 */
class UpdateCommandEvent extends BaseCommandEvent
{

    /**
     * @var array
     */
    public $columns;

}
