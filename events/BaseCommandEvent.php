<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\events;

use ox404fff\moddatabase\db\Command;
use yii\base\ModelEvent;

/**
 * Abstract command object events class
 *
 * Class BaseCommandEvent
 * @package ox404fff\moddatabase\db
 */
abstract class BaseCommandEvent extends ModelEvent
{

    /**
     * @var string
     */
    public $table;
    
    
    /**
     * @var array|string
     */
    public $condition = '';


    /**
     * @var array
     */
    public $params = [];
    
    /**
     * @var null|Command
     */
    private $_replaceCommand = null;


    /**
     * @var array
     */
    private $_originalConfig;

    
    /**
     * @inheritdoc
     * 
     * DeleteCommandEvent constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->setOriginalConfig($config);
        parent::__construct($config);
    }


    /**
     * Original construct functionality
     * 
     * @param $config
     */
    protected function configureAndInitAttributes($config)
    {
        parent::__construct($config);
    }


    /**
     * Save original config on event created
     * 
     * @param $config
     */
    protected function setOriginalConfig($config)
    {
        $this->_originalConfig = $config;
    }
    

    /**
     * Get original event config
     * 
     * @return array
     */
    public function getOriginalConfig()
    {
        return $this->_originalConfig;
    }


    /**
     * Set is command instance
     * 
     * @param Command $command
     */
    public function setReplaceCommand(Command $command)
    {
        $this->_replaceCommand = $command;
    }


    /**
     * Get is command instance
     * 
     * @return null|Command
     */
    public function getReplaceCommand()
    {
        return $this->_replaceCommand;
    }


    /**
     * Get is command replaced
     * 
     * @return bool
     */
    public function isCommandReplaced()
    {
        return !is_null($this->_replaceCommand);
    }
}
