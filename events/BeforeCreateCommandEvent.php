<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\events;


/**
 * Event on after create command in ActiveQuery class
 *
 * Class BeforeCreateCommandEvent
 * @package ox404fff\moddatabase\db
 */
class BeforeCreateCommandEvent extends BaseCreateCommandEvent
{
    
}
