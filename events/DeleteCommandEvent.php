<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\events;


/**
 * Event when deleting in Command class
 *
 * Class BaseDeleteCommandEvent
 * @package ox404fff\moddatabase\db
 */
class DeleteCommandEvent extends BaseCommandEvent
{
    
    
}
