<?php
/**
 * @link https://github.com/ox404fff/
 * @author ox404fff
 */

namespace ox404fff\moddatabase\exceptions;

class SafeDeleteActiveQueryBehaviorException extends \Exception
{
    
}